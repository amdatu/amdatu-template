/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor.test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

import junit.framework.TestCase;

import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.amdatu.template.processor.velocity.VelocityTemplateContext;
import org.amdatu.template.processor.velocity.VelocityTemplateEngine;
import org.mockito.Mockito;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class VelocityTemplateProcessorTest extends TestCase {

    private static final String LINE_DELIMITER = System.getProperty("line.separator");

    private File m_inputFile;
    private File m_baseDir;
    private URL m_inputURL;
    private Reader m_inputReader;

    private LogService m_logService;
    private BundleContext m_bundleContext;
    private TemplateContext m_context;

    public void testGenerateFileFromFile() throws IOException, TemplateException {
    	TemplateEngine eng = createEngine();
    	TemplateProcessor proc = eng.createProcessor(m_inputFile);
        File outputFile = createOutputFile();       
        proc.generateFile(m_context, outputFile);
        
        assertEquals("Bla bla lba beanValue ${ } ${a.b.c}Ab ${c} ", readAsString(new FileReader(outputFile)));
    }
    
    public void testGenerateFileFromString() throws IOException, TemplateException {
    	TemplateEngine eng = createEngine();
    	TemplateProcessor proc = eng.createProcessor("Bla bla lba $bean.getValue() ${ } ${a.b.c}${a}b ${c} ## Invisible text");
        File outputFile = createOutputFile();       
        proc.generateFile(m_context, outputFile);
        
        assertEquals("Bla bla lba beanValue ${ } ${a.b.c}Ab ${c} ", readAsString(new FileReader(outputFile)));
    }

    public void testGenerateFileFromReader() throws IOException, TemplateException {
    	TemplateEngine eng = createEngine();
    	TemplateProcessor proc = eng.createProcessor(m_inputReader);
        File outputFile = createOutputFile();       
        proc.generateFile(m_context, outputFile);
        
        assertEquals("Bla bla lba beanValue ${ } ${a.b.c}Ab ${c} ", readAsString(new FileReader(outputFile)));
    }

    public void testGenerateFileFromURL() throws IOException, TemplateException {
    	TemplateEngine eng = createEngine();
    	TemplateProcessor proc = eng.createProcessor(m_inputURL);
        File outputFile = createOutputFile();       
        proc.generateFile(m_context, outputFile);
        
        assertEquals("Bla bla lba beanValue ${ } ${a.b.c}Ab ${c} ", readAsString(new FileReader(outputFile)));
    }
    
    public void testProcessNewLine() throws TemplateException {
        TemplateEngine eng = createEngine();
        TemplateProcessor proc = eng.createProcessor("{A} $newline {B}");
        
        String outputString = proc.generateString(m_context);
        assertEquals("{A} " + LINE_DELIMITER + " {B}", outputString);
    }
    
    public void testProcessNewLineWithLineDelimiter() throws TemplateException {
        TemplateEngine eng = createEngine();
        TemplateProcessor proc = eng.createProcessor("{A} "+LINE_DELIMITER+"$newline {B}");
        
        String outputString = proc.generateString(m_context);
        assertEquals("{A} " + LINE_DELIMITER + LINE_DELIMITER + " {B}", outputString);
    }
    
    public void testCreateProcessorWithNullURL() throws Exception {
    	TemplateEngine eng = createEngine();
    	URL url = null;
    	try {
    		eng.createProcessor(url);
    		fail("Should not be able to create processor with null URL");
    	} catch (NullPointerException e) {
    		assertEquals("templateURL cannot be null", e.getMessage());
    	}
    }
    
    public void testCreateProcessorWithNullString() throws Exception {
    	TemplateEngine eng = createEngine();
    	String template = null;
    	try {
    		eng.createProcessor(template);
    		fail("Should not be able to create processor with null URL");
    	} catch (NullPointerException e) {
    		assertEquals("template cannot be null", e.getMessage());
    	}
    }
    
    public void testCreateProcessorWithNullReader() throws Exception {
    	TemplateEngine eng = createEngine();
    	InputStreamReader reader = null;
    	try {
    		eng.createProcessor(reader);
    		fail("Should not be able to create processor with null URL");
    	} catch (NullPointerException e) {
    		assertEquals("templateStreamReader cannot be null", e.getMessage());
    	}
    }
    
    public void testCreateProcessorWithNullFile() throws Exception {
    	TemplateEngine eng = createEngine();
    	File file = null;
    	try {
    		eng.createProcessor(file);
    		fail("Should not be able to create processor with null URL");
    	} catch (NullPointerException e) {
    		assertEquals("file cannot be null", e.getMessage());
    	}
    }

    protected void setUp() throws Exception {
        m_baseDir = File.createTempFile("vtpt", "");
        m_baseDir.delete();
        m_baseDir = m_baseDir.getParentFile();

        String inputFileName = "template.txt";

        m_inputURL = VelocityTemplateProcessorTest.class.getResource(inputFileName);
        assertNotNull("Failed to find local resource: template.txt?!", m_inputURL);

        m_inputReader = new InputStreamReader(m_inputURL.openStream());

        m_inputFile = new File(m_inputURL.toURI());

        m_logService = Mockito.mock(LogService.class);
        m_bundleContext = Mockito.mock(BundleContext.class);
        Mockito.when(m_bundleContext.getDataFile("work")).thenReturn(m_baseDir);

        createContext();
    }

    protected void tearDown() throws Exception {
        m_inputReader.close();
    }

    private void createContext() {
        m_context = new VelocityTemplateContext();
        m_context.put("a", "A");
        m_context.put("a.b", "AB");
        m_context.put("a.b.c", "ABC");
        m_context.put("newline", LINE_DELIMITER);
        m_context.put("bean", new VelocityBean());
    }

    private TemplateEngine createEngine() {
        VelocityTemplateEngine engine = new VelocityTemplateEngine(m_logService, m_bundleContext);
        engine.init();
        return engine;
    }

    private File createOutputFile() throws IOException {
        File outputFile = File.createTempFile("test", ".txt");
        outputFile.deleteOnExit();
        return outputFile;
    }

    private String readAsString(Reader br) throws IOException {
        StringBuilder sb = new StringBuilder();
        
        int ch;
        while ((ch = br.read()) >= 0) {
            sb.append((char) ch);
        }
        br.close();
        
        return sb.toString();
    }

}