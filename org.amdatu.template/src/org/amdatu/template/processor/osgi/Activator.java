/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor.osgi;

import java.util.Properties;

import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.velocity.VelocityTemplateEngine;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * This is the activator for the template processor bundle.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {

    @SuppressWarnings("serial")
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent()
            .setInterface(TemplateEngine.class.getName(), new Properties() {
                {
                    put(TemplateEngine.KEY_TEMPLATE_ENGINE_TYPE, VelocityTemplateEngine.ENGINE_TYPE);
                    put(TemplateEngine.KEY_TEMPLATE_ENGINE_VERSION, VelocityTemplateEngine.ENGINE_VERSION);
                }
            })
            .setImplementation(VelocityTemplateEngine.class)
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {

    }
}
