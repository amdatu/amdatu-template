/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor;

import java.io.File;
import java.io.Reader;
import java.net.URL;

/**
 * A template engine is registered as a service, with its name and version as properties of this
 * service. This allows us to have multiple engines registered, all using the same API, but you can
 * still depend on an engine that you're compatible with by being specific about these properties.
 * Each template engine allows you to first create a template based on some URL. This step allows
 * the engine to preprocess an incoming template. The second step is to create a template context,
 * which is an object that holds all values that you want to use when converting the template into a
 * concrete document (or whatever stream comes out of the template). Once you have both the template
 * and the context, you can invoke the generation of these documents on the template. Several ways
 * of creating the template processor are available.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface TemplateEngine {

    String KEY_TEMPLATE_ENGINE_TYPE = "org.amdatu.template.engine.name";
    String KEY_TEMPLATE_ENGINE_VERSION = "org.amdatu.template.engine.version";

    /**
     * Creates a processor with the template found at <i>templateURL</i>. If the URL is invalid or
     * for some other reason unreachable, an IllegalArgumentException is thrown.
     * 
     * @param templateURL The URL of the template
     * @return A processor that can process the template
     */
    TemplateProcessor createProcessor(URL templateURL) throws TemplateException;

    /**
     * Creates a processor with the specified String as plain template.
     * 
     * @param template The template itself
     * @return A processor that can process the template
     */
    TemplateProcessor createProcessor(String template) throws TemplateException;

    /**
     * Creates a processor with the template found 'in' the <i>templateStream</i>. If the template could not
     * be read from the Reader an IllegalArgumentException is thrown.
     * 
     * @param templateStream The Reader that streams the template
     * @return A processor that can process the template
     */
    TemplateProcessor createProcessor(Reader templateStream) throws TemplateException;

    /**
     * Creates a processor with the template found in <i>templateFile</i>. If the File is invalid or
     * for some other reason unreachable, an IllegalArgumentException is thrown.
     * 
     * @param templateFile The File with the template
     * @return A processor that can process the template
     */
    TemplateProcessor createProcessor(File templateFile) throws TemplateException;

    /**
     * Creates a default context you can use to generate the template.
     * 
     * @return The context
     */
    TemplateContext createContext();
}
