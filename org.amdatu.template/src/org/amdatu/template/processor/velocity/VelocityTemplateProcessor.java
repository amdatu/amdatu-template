/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor.velocity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;

import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;

/**
 * This implementation of TemplateProcessor is based on the the Velocity engine, meaning that the
 * template will be passed through to Velocity, which generates the desired result. It provides the
 * functionality to either pass Velocity a template at once, or stream it instead.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class VelocityTemplateProcessor implements TemplateProcessor {

    private static final String LINE_DELIMITER = System.getProperty("line.separator");

    private final Reader m_streamTemplate;
    private final Template m_velocityTemplate;
    private final File m_workDir;

    /**
     * Creates a new VelocityTemplateProcessor.
     */
    public VelocityTemplateProcessor() {
        m_streamTemplate = null;
        m_velocityTemplate = null;
        m_workDir = null;
    }

    /**
     * Creates a new template processor using the Velocity engine.
     * 
     * @param velocityTemplate The Velocity template
     * @param workDir The working directory
     */
    public VelocityTemplateProcessor(Template velocityTemplate, File workDir) {
        m_streamTemplate = null;
        m_velocityTemplate = velocityTemplate;
        m_workDir = workDir;
    }

    /**
     * Creates a new TemplateProcessor using the Velocity engine. Instead of providing a
     * template this constructor allows you to provide an InputStream, which will be used
     * instead by Velocity. This way, the template can be streamed as you like, instead of
     * providing everything at once.
     * 
     * @param templateStream The template stream
     * @param workDir The working directory
     */
    public VelocityTemplateProcessor(Reader templateStream, File workDir) {
        m_streamTemplate = templateStream;
        m_velocityTemplate = null;
        m_workDir = workDir;
    }

    /**
     * {@inheritDoc}
     */
    public String generateString(TemplateContext context) throws TemplateException {
        URL fileUrl = generateURL(context);
        try {
            return read(new BufferedReader(new InputStreamReader((InputStream) fileUrl.getContent())));
        }
        catch (IOException ioe) {
            throw new TemplateException("Unable to generate template", ioe);
        }
    }

    /**
     * {@inheritDoc}
     */
    public URL generateURL(TemplateContext context) throws TemplateException {
        // Determine the filename for the converted file
        // Create the temporary file to write the converted configuration template to
        File configFile;
        try {
            configFile = File.createTempFile("amdatu_ctp", ".cfg", m_workDir);
            configFile.deleteOnExit();
        }
        catch (IOException ioe) {
            throw new TemplateException("Could not create temporary file", ioe);
        }

        // Write the file with replaced configuration entries to the target file
        generateFile(context, configFile);

        // Return the new file's URL
        try {
            return configFile.toURI().toURL();
        }
        catch (IOException ioe) {
            throw new TemplateException("Unable to generate template", ioe);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void generateFile(TemplateContext context, File targetFile) throws TemplateException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(targetFile));
            generateStream(context, writer);
        }
        catch (IOException ioe) {
            throw new TemplateException("Unable to generate template", ioe);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void generateStream(TemplateContext context, Writer writer) throws TemplateException {
        try {
            if (m_velocityTemplate != null) {
                m_velocityTemplate.merge(((VelocityTemplateContext) context).getContext(), writer);
            }
            else if (m_streamTemplate != null) {
                Velocity.evaluate(((VelocityTemplateContext) context).getContext(), writer, "GEN_TEMP",
                    m_streamTemplate);
            }
            else {
                throw new TemplateException();
            }

            writer.flush();
        }
        catch (IOException ioe) {
            throw new TemplateException("Unable to generate template", ioe);
        }
    }

    /**
     * Reads all data from the specified buffered reader and returns a String with this data.
     * 
     * @param br The BufferedReader
     * @return The contents
     * @throws IOException If an error occurs while reading the stream
     */
    private static String read(BufferedReader br) throws IOException {
        String line = br.readLine();
        String result = line;
        while ((line = br.readLine()) != null) {
            result += LINE_DELIMITER;
            result += line;
        }
        br.close();
        return result;
    }

}
