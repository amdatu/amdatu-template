/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor.velocity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.Objects;
import java.util.Properties;

import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.resource.loader.URLResourceLoader;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * This implementation of TemplateEngine is based on the the Velocity engine, meaning that the
 * template will be passed through to Velocity, which generates the desired result. It provides the
 * functionality to either pass Velocity a template at once, or stream it instead (if, of course, you
 * create the processor using a stream).
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class VelocityTemplateEngine implements TemplateEngine {

    public static final String ENGINE_TYPE = "velocity";
    public static final String ENGINE_VERSION = "1.7";

    // Services injected by the Felix dependency manager
    private volatile LogService m_logService;
    private volatile BundleContext m_bundleContext;

    // Configured properties
    private String m_workDirName = "";

    private File m_workDir;

    /**
     * Method invoked by the Felix dependency manager. Initializes the engine.
     */
    public void init() {
        initWorkDir();

        Properties p = new Properties();

        p.setProperty("resource.loader", "url");
        p.setProperty("url.resource.loader.class", URLResourceLoader.class.getName());
        p.setProperty("url.resource.loader.root", "");
        p.setProperty("url.resource.loader.cache", "true");
        p.setProperty("url.resource.loader.modificationCheckInterval", "3");

        p.setProperty("runtime.log.logsystem.class", org.apache.velocity.runtime.log.NullLogChute.class.getName());
        p.setProperty("runtime.log.logsystem.root", "");

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
        try {
            Velocity.init(p);
        }
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }

        m_logService.log(LogService.LOG_INFO, "Service " + getClass().getName() + " initialized");
    }

    /**
     * Constructs a default Velocity engine.
     */
    public VelocityTemplateEngine() {
    }

    /**
     * Creates a new engine with the provided LogService. Used for testing.
     * 
     * @param logService The LogService
     * @param bundleContext The BundleContext
     */
    public VelocityTemplateEngine(LogService logService, BundleContext bundleContext) {
        m_logService = logService;
        m_bundleContext = bundleContext;
    }

    /**
     * {@inheritDoc}
     */
    public TemplateProcessor createProcessor(URL templateURL) throws TemplateException {
        Objects.requireNonNull(templateURL, "templateURL cannot be null");
        return new VelocityTemplateProcessor(Velocity.getTemplate(templateURL.toExternalForm()), m_workDir);
    }

    /**
     * {@inheritDoc}
     */
    public TemplateProcessor createProcessor(String template) throws TemplateException, IllegalArgumentException {
    	Objects.requireNonNull(template, "template cannot be null");
        File tmp;
        try {
            tmp = File.createTempFile("amdatu_vte", ".tmp", m_workDir);
            BufferedWriter br = new BufferedWriter(new FileWriter(tmp));
            br.write(template);
            br.close();
        }
        catch (IOException ioe) {
            throw new TemplateException("Could not create temporary file", ioe);
        }
        return createProcessor(tmp);
    }

    /**
     * {@inheritDoc}
     */
    public TemplateProcessor createProcessor(Reader templateStreamReader) throws TemplateException {
    	Objects.requireNonNull(templateStreamReader, "templateStreamReader cannot be null");
        return new VelocityTemplateProcessor(templateStreamReader, m_workDir);
    }

    /**
     * {@inheritDoc}
     */
    public TemplateProcessor createProcessor(File file) throws TemplateException {
    	Objects.requireNonNull(file, "file cannot be null");
        try {
            return createProcessor(file.toURI().toURL());
        }
        catch (IOException ioe) {
            throw new TemplateException("Could not create processor", ioe);
        }
    }

    /**
     * {@inheritDoc}
     */
    public TemplateContext createContext() {
        return new VelocityTemplateContext(new VelocityContext());
    }

    /**
     * Finds and creates the working directory for the caching template engine. The working
     * directory is used by - for example - the processor when creating temporary files and
     * it is used for the caching files. The default location of the working directory is the
     * path <i>System.getProperty("amdatu.dir")</i> returns and in that location the folder
     * 'work'. If this returns null the default user directory is used (property "user.dir").
     * The used folder is logged.
     */
    private void initWorkDir() {
        File workBaseDir = m_bundleContext.getDataFile("work");
        m_workDir = new File(workBaseDir, m_workDirName);
        m_workDir.mkdirs();
        m_logService.log(LogService.LOG_INFO, "CachingTemplateManager work directory = " + m_workDir.getAbsolutePath());
    }

}
