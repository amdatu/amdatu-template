/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor.velocity;

import java.util.HashSet;
import java.util.Set;

import org.amdatu.template.processor.TemplateContext;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;

/**
 * This implementation of TemplateContext uses Velocity's Context as back-end.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class VelocityTemplateContext implements TemplateContext {
    private final Context m_context;

    /**
     * Creates a default, empty, VelocityTemplateContext with a VelocityContext as it's
     * implementation.
     */
    public VelocityTemplateContext() {
        m_context = new VelocityContext();
    }

    /**
     * Creates a VelocityTemplateContext with the specified context as the context of this
     * class.
     * 
     * @param context The context to use
     */
    public VelocityTemplateContext(Context context) {
        m_context = context;
    }

    /**
     * Returns the used context.
     * 
     * @return The context
     */
    public Context getContext() {
        return m_context;
    }

    /**
     * {@inheritDoc}
     */
    public boolean containsKey(String key) {
        return m_context.containsKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public Object get(String key) {
        return m_context.get(key);
    }

    /**
     * {@inheritDoc}
     */
    public Set<String> getKeys() {
        Object[] keys = m_context.getKeys();
        HashSet<String> set = new HashSet<String>();
        for (Object key : keys) {
            set.add((String) key);
        }
        return set;
    }

    /**
     * {@inheritDoc}
     */
    public Object put(String key, Object value) {
        return m_context.put(key, value);
    }

    /**
     * {@inheritDoc}
     */
    public Object remove(String key) {
        return m_context.remove(key);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEmpty() {
        return m_context.getKeys().length == 0;
    }
}
