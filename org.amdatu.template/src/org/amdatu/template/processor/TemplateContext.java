/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor;

import java.util.Set;

/**
 * The TemplateProcessor tries to generate an output file given an input file and a dictionary that
 * is to be used to match the configuration entries in the template. The TemplateContext is such a
 * dictionary and provides the functionality a very basic Map<String,Object> does. Each key in the
 * context is a String and it is associated with a value, being an Object.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface TemplateContext {
    /**
     * Checks if the specified key is used in this context.
     * 
     * @param key The key to check for
     * @return If the key is found true, false otherwise
     */
    boolean containsKey(String key);

    /**
     * Checks if this context contains data.
     * 
     * @return If it does, return false, true otherwise
     */
    boolean isEmpty();

    /**
     * Returns the Object matching <i>key</i>.
     * 
     * @param key The key
     * @return The value (an Object)
     */
    Object get(String key);

    /**
     * Returns a Set of Strings containing all keys.
     * 
     * @return The keys in a set
     */
    Set<String> getKeys();

    /**
     * Puts an object in the context. If a <Key, Value> pair already exists with the specified key,
     * the old value is overwritten.
     * 
     * @param key The key
     * @param value The value
     * @return The value that was previously associated with the key, or null otherwise
     */
    Object put(String key, Object value);

    /**
     * Removes the object associated with <i>key</i> from the context.
     * 
     * @param key The key
     * @return The object that was associated with the key, or null if the key didn't exist.
     */
    Object remove(String key);

}
