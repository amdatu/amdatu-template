== Dependencies

The following table provides an overview of the required and optional dependencies for Amdatu Template:

[cols="40%,10%,50%"]
|===
| Bundle
	| Required
	| Description
| org.apache.felix.dependencymanager
	| yes
	| Apache Felix Dependency Manager is used internally by all Amdatu components
|===
